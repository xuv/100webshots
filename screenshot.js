const puppeteer = require('puppeteer');
const fs = require('fs');

(async () => {
  createDirectory();
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.setViewport({
    width: 1920,
    height: 1080,
    deviceScaleFactor: 1,
  });
  await page.goto('https://example.com');
  await page.screenshot({ path: currentDate() + '/example.png' });

  await browser.close();
})();


function currentDate() {
    let date = new Date()
    return date.toISOString().slice(0,10); 
} 

function createDirectory() {
    const dir = "./" + currentDate();
    // create new directory
    try {
        // first check if directory already exists
        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir);
            console.log("Directory is created.");
        } else {
            console.log("Directory already exists.");
        }
    } catch (err) {
        console.log(err);
}
}